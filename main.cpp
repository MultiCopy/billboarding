#include <QtWidgets>
//#include <QtGui/QGuiApplication>
#include <OpenGLWindow.h>
#include <geSG/Scene.h>
#include <geSG/DefaultImage.h>
#include <QtImageLoader.h>
#include <AssimpModelLoader.h>
#include <QFileInfo>

#include <GLWidget.h>

/**
* Simple function that loads images that are found at image components of materials
* and populate its image property.
*
* \warning Assumes relative texture file paths or empty imageDir. There should be an extended check
*          in production code.
*/
void loadImages(ge::sg::Scene* scene, std::string& imageDir)
{
	std::shared_ptr<ge::sg::DefaultImage> defaultImage(std::make_shared<ge::sg::DefaultImage>());

	for (auto model : scene->models)
	{
		for (std::shared_ptr<ge::sg::Material> material : model->materials)
		{
			for (std::vector<std::shared_ptr<ge::sg::MaterialComponent>>::iterator it = material->materialComponents.begin(); it != material->materialComponents.end(); ++it)
			{
				if ((*it)->getType() == ge::sg::MaterialComponent::ComponentType::IMAGE)
				{
					ge::sg::MaterialImageComponent *img = dynamic_cast<ge::sg::MaterialImageComponent*>((*it).get());
					//cout << img->semantic << " " << img->filePath << endl;
					std::string textFile(imageDir + img->filePath);
					std::shared_ptr<QtImage> image(QtImageLoader::loadImage(textFile.c_str()));
					if (image == nullptr)
					{
						std::cout << imageDir << img->filePath << std::endl;
						std::cout << "  " << "FAILED TO LOAD!" << "substituting default image\n";
						img->image = defaultImage;
					}
					else {
						img->image = image;
					}
				}
			}
		}
	}
}

//! [main]
int main(int argc, char **argv)
{
	 
   QApplication app(argc, argv);
   
   app::mw::OpenGLWindow* glWindow = new app::mw::OpenGLWindow();
   //window.resize(800, 600);

   //model loading
   QString modelFileName(APP_RESOURCES"/models/Tree/tree.obj");
   QFileInfo fi(modelFileName);
   std::string modelPath(qUtf8Printable(fi.canonicalPath() + "/"));

   std::shared_ptr<ge::sg::Scene> scene(AssimpModelLoader::loadScene(modelFileName.toUtf8().constData()));
   if (!scene) {
	   std::cerr << "Failed to load scene!" << std::endl;
   }
   glWindow->setScene(scene);

   //load images
   loadImages(scene.get(), modelPath);
   
   app::mw::GLWidget window(glWindow);
   window.resize(900, 700);
   
   window.show();
   
   return app.exec();
}
//! [main]