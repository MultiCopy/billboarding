#include <RenderManager.h>

#include <geGL/geGL.h>
#include <geGL/Texture.h>

#include <QtImageLoader.h>

#include <glsg/EnumToGL.h>

#include <glm/gtc/matrix_transform.hpp>

using namespace std;
using namespace glm;
using namespace app::mw;
using namespace ge::gl;
using namespace ge::glsg;

int RenderManager::state = default;
RenderManager::ImpostorInfo RenderManager::impostorInfo;

void RenderManager::render()
{
	
}


shared_ptr<Texture> RenderManager::loadTexture(QString textPath, shared_ptr<Context> gl) {

	std::shared_ptr<QtImage> image(QtImageLoader::loadImage(textPath));
	if (image == nullptr) {
		std::cerr << "  " << "FAILED TO LOAD TEXTURE!\n";
	}

	int w = (int)image->getWidth();
	int h = (int)image->getHeight();
	int l = 0;
	while (h > 0 || w > 0)
	{
		h >>= 1;
		w >>= 1;
		++l;
	}

	shared_ptr<Texture> tex(make_shared<Texture>(GL_TEXTURE_2D, GL_RGBA8, l, (GLsizei)image->getWidth(), (GLsizei)image->getHeight()));
	gl->glTextureSubImage2DEXT(tex->getId(), GL_TEXTURE_2D, 0, 0, 0, (GLsizei)image->getWidth(), (GLsizei)image->getHeight(), translateEnum(image->getFormat()), translateEnum(image->getDataType()), image->getBits());
	gl->glGenerateTextureMipmap(tex->getId());
	gl->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	gl->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	tex->unbind(0);

	return tex;
}

void RenderManager::setImpostorInfo(int numberOfRows) { impostorInfo.numberOfRows = numberOfRows; }
void RenderManager::setRenderingState(int state) { RenderManager::state = state; }

void RenderManager::setUpGLState(shared_ptr<Context> gl, vec2 viewPortRes)
{
	gl->glViewport(0, 0, viewPortRes.x, viewPortRes.y);
	gl->glClearColor(0.1, 0.1, 0.1, 0.0);
	gl->glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}
