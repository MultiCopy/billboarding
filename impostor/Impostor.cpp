#include <impostor/Impostor.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <geGL/Texture.h>
#include <geGL/Buffer.h>
#include <geGL/VertexArray.h>
#include <geGL/Program.h>
#include <geGL/OpenGLContext.h>

#include <glsg/GLModel.h>

using namespace std;
using namespace app::mi;
using namespace glm;
using namespace ge::gl;
using namespace ge::glsg;

const vector<float> Impostor::squarePos = {
	-1.0f,  1.0f, 0.0f, 1.0f, 1.0f, // 0
	1.0f,  1.0f, 0.0f, 0.0f, 1.0f, // 1
	1.0f, -1.0f, 0.0f, 0.0f, 0.0f, // 2
	-1.0f, -1.0f, 0.0f, 1.0f, 0.0f  // 3
};

const vector<int> Impostor::squareIndices = {
	0, 1, 2,
	2, 3, 0
};

const string Impostor::computeTextureIndex =
	R".(
		uniform int numberOfRows;
		uniform float rotationXZ = 0.0f;

		float calculateImpostorLayer(mat4 view) 
		{
			
			vec3 up_vector = vec3(view[0][1], view[1][1], view[2][1]);
			mat4 invView = inverse(view);
			vec3 eye_position = vec3(invView[3][0], invView[3][1], invView[3][2]);
			vec3 forward_vector = eye_position - billboard_position;
			forward_vector = normalize(forward_vector);
			vec3 right_vector = cross(up_vector, forward_vector);

			float offset = 3.14f / numberOfRows;
			// from [-pi, pi] to [-1, 1]
			
			vec2 normFront = vec2(right_vector.x, right_vector.z);
			float horizontalRad = (atan(normFront.x, normFront.y) + offset) / 3.14f;
			
			float horizontalRotation = radians(rotationXZ) / 6.28f;
			horizontalRad = (horizontalRad + 1) * 0.5f + horizontalRotation; // from [-1, 1] to [0, 1]
			//rad = 1 - rad; //flyp purposes
			float textureIndexX = floor(horizontalRad * numberOfRows) - 1; // view index
			textureIndexX = mod(textureIndexX, numberOfRows);
			
			float verticalRotation = radians(0.0f);
			float verticalRad = acos(forward_vector.y / length(forward_vector)) + verticalRotation;
			float textureIndexY = mod(floor(verticalRad / offset), numberOfRows);
		
			return (textureIndexY * numberOfRows) + textureIndexX;	
		}
	).";

Impostor::Impostor(GPUBillboard billboard, shared_ptr<Context> gl)
	: GPUBillboard(billboard)
	, gl(gl)
	, VAO(make_shared<VertexArray>())
{
	init();
}

Impostor::Impostor(GPUBillboard billboard, shared_ptr<Context> gl, shared_ptr<Texture> textureArray, int numberOfRows)
	: GPUBillboard(billboard)
	, gl(gl)
	, textureArray(textureArray)
	, numberOfRows(numberOfRows)
	, VAO(make_shared<VertexArray>())
{
	init();
}

Impostor::Impostor(GPUBillboard billboard, shared_ptr<Context> gl, shared_ptr<GLModel> glmodel, shared_ptr<Texture> textureArray, int numberOfRows)
	: GPUBillboard(billboard)
	, gl(gl)
	, glmodel(glmodel)
	, textureArray(textureArray)
	, numberOfRows(numberOfRows)
	, VAO(make_shared<VertexArray>())
{
	init();
}

void Impostor::init() {
	
	this->vector = vec3(0.0f, 1.0f, 0.0f);
	this->mode = BILLBOARD_AXIAL_Y;
	
	shared_ptr<Buffer> positions = make_shared<Buffer>(squarePos.size() * sizeof(float), squarePos.data()/*, GL_STATIC_DRAW */);
	shared_ptr<Buffer> elementBuffer = make_shared<Buffer>(squareIndices.size() * sizeof(int), squareIndices.data()/*, GL_STATIC_DRAW */);
	
	VAO->bind();
	VAO->addElementBuffer(elementBuffer);
	VAO->addAttrib(positions, 0, 3, GL_FLOAT, 5 * sizeof(float), 0);
	VAO->addAttrib(positions, 1, 2, GL_FLOAT, 5 * sizeof(float), 3 * sizeof(float));
	VAO->unbind();
}


void Impostor::recomputeImpostor(const mat4& view, mat4& worldMatrix)
{
	this->setPointSprite(view);
	this->recompute(view, worldMatrix);
}

void Impostor::draw(const mat4& view, mat4& worldMatrix)
{
	this->getShaderProgram()->use();
	getShaderProgram()->set1i("numberOfRows", numberOfRows);
	
	recomputeImpostor(view, worldMatrix);
	VAO->bind();
	textureArray->bind(0);
	gl->glDrawElements(GL_TRIANGLE_FAN, 6, GL_UNSIGNED_INT, nullptr);
	textureArray->unbind(0);
	VAO->unbind();
}

/// getters & setters
shared_ptr<Texture> Impostor::getTextureArray() { return this->textureArray; }
int Impostor::getNumberOfRows() { return numberOfRows; }
void Impostor::setTextureArray(shared_ptr<Texture> textureArray, int numberOfRows) { this->textureArray = textureArray; this->numberOfRows = numberOfRows; }

shared_ptr<ge::glsg::GLModel> Impostor::getGLModel() { return this->glmodel; }
void Impostor::setGLModel(shared_ptr<ge::glsg::GLModel> glmodel) { this->glmodel = glmodel; }

