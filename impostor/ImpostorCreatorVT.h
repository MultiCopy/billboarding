#pragma once

#include <memory>
#include <unordered_map>

#include <QTgeSG/VisualizationTechnique.h>

#include <geSG/AttributeDescriptor.h>
#include <glsg/GLSceneProcessor.h>

namespace ge 
{
	namespace glsg
	{
		class GLModel;
	}

	namespace gl
	{
		class Context;
		class Program;
		class VertexArray;
		class Texture;
	}

	namespace sg
	{
		class Model;
	}
}

namespace app
{
	namespace mi
	{
		class ImpostorCreatorVT : public fsg::VisualizationTechnique
		{
		public:
			virtual void setModel(std::shared_ptr<ge::glsg::GLModel>& model);
			virtual void processModel();
			void drawSetup() override;
			void draw() override;

			static int semantic2Attribute(ge::sg::AttributeDescriptor::Semantic semantic);

			std::shared_ptr<ge::gl::Context> gl;
			std::shared_ptr<ge::gl::Program> program;

		protected:
			std::shared_ptr<ge::glsg::GLModel> glmodel;
			std::unordered_map<ge::sg::Mesh*, std::shared_ptr<ge::gl::VertexArray>> VAOContainer;
			std::unordered_map<ge::sg::Material*, std::unique_ptr<unsigned char[]>> colorContainer;
			std::unordered_map<ge::sg::Mesh*, std::shared_ptr<ge::gl::Texture>> diffuseTextureConatiner;
		};
	}
}
