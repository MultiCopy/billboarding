#pragma once


#pragma once

#include <Billboarding/GPUBillboard.h>
#include <string>
#include <vector>
#include <geGL/OpenGLContext.h>

namespace ge {

	namespace gl {
		class Context;
		class Program;
		class Texture;
		class VertexArray;
	}

	namespace glsg {
		class GLModel;
	}
};

namespace app
{
	namespace mi
	{
		class Impostor : public ge::ad::GPUBillboard {

			public:
				static const std::string computeTextureIndex;

				static const std::vector<float> squarePos;
				static const std::vector<int> squareIndices;

				std::shared_ptr<ge::gl::Context> gl;

				Impostor(GPUBillboard billboard, std::shared_ptr<ge::gl::Context> gl);
				Impostor(GPUBillboard billboard, std::shared_ptr<ge::gl::Context> gl, std::shared_ptr<ge::gl::Texture> textureArray, int numberOfRows);
				Impostor(GPUBillboard billboard, std::shared_ptr<ge::gl::Context> gl, std::shared_ptr<ge::glsg::GLModel> model, std::shared_ptr<ge::gl::Texture> textureArray, int numberOfRows);
				
				void recomputeImpostor(const glm::mat4& view, glm::mat4& worldMatrix);
				void draw(const glm::mat4& view, glm::mat4& worldMatrix);

				/// getters & setters;
				std::shared_ptr<ge::gl::Texture> getTextureArray();
				int getNumberOfRows();
				void setTextureArray(std::shared_ptr<ge::gl::Texture> textureArray, int numberOfRows);
				std::shared_ptr<ge::glsg::GLModel> getGLModel();
				void setGLModel(std::shared_ptr<ge::glsg::GLModel> glmodel);

			private:
				int numberOfRows;
				std::shared_ptr<ge::gl::Texture> textureArray;
				std::shared_ptr<ge::glsg::GLModel> glmodel;
				std::shared_ptr<ge::gl::VertexArray> VAO;

				void init();

		};
	};
}