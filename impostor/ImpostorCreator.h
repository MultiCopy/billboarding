#pragma once

#include <memory>
#include <string>
#include <glm/glm.hpp>

namespace ge
{
	namespace gl
	{
		class Program;
		class Texture;
		class Context;
	}

	namespace glsg
	{
		class GLModel;
	}
}

namespace app
{
	namespace mi
	{
		class Impostor;
		class ImpostorCreatorVT;

		class ImpostorCreator {

			public:
				enum CoordsType {SPHERICAL, CYLINDRICAL};

				static const std::string vertexShader;
				static const std::string fragmentShader;

				ImpostorCreator(std::shared_ptr<ge::gl::Context> gl);

				std::shared_ptr<Impostor> createImpostor(std::shared_ptr<ge::gl::Program> impostorProgram, std::shared_ptr<ge::glsg::GLModel> glmodel, glm::vec2 textureResolution, int numberOfRows, const int COORD_TYPE);

				/// getters & setters
				std::shared_ptr<ge::gl::Program> getShaderProgram();
				void setShaderProgram(std::shared_ptr <ge::gl::Program> shaderProgram);
				std::shared_ptr<ge::gl::Context> getContext();
				void setContext(std::shared_ptr<ge::gl::Context> gl);

			private:
				std::shared_ptr<ge::gl::Program> shaderProgram;
				std::shared_ptr<ge::gl::Context> gl;
				std::shared_ptr<ImpostorCreatorVT> VT;

				std::shared_ptr<ge::gl::Texture> setUpTexture(glm::vec2 textureResolution, int numberOfRows);
				void setOpenGLState(glm::vec2 textureResolution);
		};
	}
}
