#include <impostor/ImpostorCreatorVT.h>

#include <geGL/VertexArray.h>
#include <geGL/Texture.h>
#include <geGL/geGL.h>

#include <geSG/AABB.h>
#include <geSG/Material.h>

#include <glsg/EnumToGL.h>
#include <glsg/GLModel.h>

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.inl>

using namespace std;
using namespace glm;
using namespace ge::gl;
using namespace ge::sg;
using namespace ge::glsg;
using namespace app::mi;

/**
* Sets the scene to visualize.
* \param scene GLScene to visualize.
*/
void ImpostorCreatorVT::setModel(std::shared_ptr<ge::glsg::GLModel>& scene)
{
	glmodel = scene;
}

/**
* Creates VAOs, preps uniforms and textures that the VT needs for visualization.
*/
void ImpostorCreatorVT::processModel()
{
	glmodel->aabb = make_shared<AABB>();

	for (auto& meshIt : glmodel->GLMeshes)
	{
		//mesh geometry
		shared_ptr<VertexArray> VAO = make_shared<VertexArray>(gl->getFunctionTable());
		for (auto& glattrib : meshIt.second)
		{

			if (glattrib.attributeDescriptor->semantic == AttributeDescriptor::Semantic::indices)
			{
				VAO->addElementBuffer(glattrib.BO);
			}
			else
			{

				int attribLocation = semantic2Attribute(glattrib.attributeDescriptor->semantic);
				if (attribLocation != -1)
				{
					VAO->addAttrib(glattrib.BO, attribLocation, glattrib.attributeDescriptor->numComponents, translateEnum(glattrib.attributeDescriptor->type), (GLsizei)glattrib.attributeDescriptor->stride);
				}

				if (glattrib.attributeDescriptor->semantic == ge::sg::AttributeDescriptor::Semantic::position) {
					float* data = new float[glattrib.BO->getSize() / sizeof(float)];
					glattrib.BO->getData(data, glattrib.BO->getSize(), 0);

					for (int i = 3; i < (glattrib.BO->getSize() / sizeof(float)); i += 3) {
						glmodel->aabb->expand(vec3(data[i], data[i + 1], data[i + 2]));
					}
				}
			}
		}
		VAOContainer[meshIt.first] = VAO;

		//material
		Material * mat = meshIt.first->material.get();
		//one way to get the component you want but the more frequently used way is bellow
		auto component = mat->getComponent<MaterialSimpleComponent>(MaterialSimpleComponent::Semantic::diffuseColor);
		if (component)
		{
			unsigned sizeinbytes = component->size * component->getSize(component->dataType);
			colorContainer[mat] = make_unique<unsigned char[]>(sizeinbytes);
			//memcpy(colorContainer[mat].get(), component->data.get(), sizeinbytes); //this is the same as below but less general
			std::copy_n(component->data.get(), sizeinbytes, colorContainer[mat].get());
		}

		for (auto& comp : mat->materialComponents)
		{
			if (comp->getType() == MaterialComponent::ComponentType::IMAGE)
			{
				auto imageComponent = static_cast<MaterialImageComponent*>(comp.get());
				diffuseTextureConatiner[meshIt.first] = glmodel->textures[imageComponent];
			}
		}
	}

	vec3 max = glmodel->aabb->max;
	vec3 min = glmodel->aabb->min;
	float heightX = min.x < 0 ? max.x + (-min.x) : max.x - min.x;
	float heightY = min.y < 0 ? max.y + (-min.y) : max.y - min.y;
	float heightZ = min.z < 0 ? max.z + (-min.z) : max.z - min.z;

	glmodel->maxSize = heightX;
	if (glmodel->maxSize < heightY) glmodel->maxSize = heightY;
	else if (glmodel->maxSize < heightZ) glmodel->maxSize = heightZ;

	//glmodel->maxSize = distance(glmodel->aabb->max, glmodel->aabb->min);
}

/**
* Currently does nothing.
*/
void ImpostorCreatorVT::drawSetup()
{
}

/**
* Use provided shader and draws the provided scene.
*/
void ImpostorCreatorVT::draw()
{
	program->use();
	if (!glmodel) return;
	for (auto& meshIt : glmodel->GLMeshes)
	{
		Mesh* mesh = meshIt.first;

		Texture *texture = diffuseTextureConatiner[meshIt.first].get();
		texture->bind(0);

		VertexArray * VAO = VAOContainer[mesh].get();
		VAO->bind();
		gl->glDrawElements(translateEnum(mesh->primitive), mesh->count, translateEnum(mesh->getAttribute(AttributeDescriptor::Semantic::indices)->type), 0);
		VAO->unbind();
	}
}

/**
* Internal helper function that returns attribute position for given semantic or -1
* if the attrib is not to be used by this VT.
*/
int ImpostorCreatorVT::semantic2Attribute(AttributeDescriptor::Semantic semantic)
{
	switch (semantic)
	{
	case AttributeDescriptor::Semantic::position: return 0;
	case AttributeDescriptor::Semantic::texcoord: return 2;
	default: return -1;
	}
}
