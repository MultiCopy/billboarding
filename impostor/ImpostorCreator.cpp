#include <impostor/ImpostorCreator.h>

#include <impostor/ImpostorCreatorVT.h>
#include <impostor/Impostor.h>

#include <glsg/GLModel.h>

#include <geGL/Program.h>
#include <geGL/Texture.h>
#include <geGL/Renderbuffer.h>
#include <geGL/Framebuffer.h>

#include <glm/gtc/matrix_transform.inl>
#include <glm/gtc/type_ptr.hpp>

using namespace app::mi;
using namespace ge::gl;
using namespace ge::glsg;
using namespace glm;
using namespace std;
using namespace ge::ad;

const string ImpostorCreator::vertexShader =  
	R".(
		#version 330
		in layout(location = 0) vec3 position;
		in layout(location = 2) vec2 vertexUV;
		out vs_output_interface {
		    vec2 UV;
		}attribs;
		uniform mat4 model;
		void main() {
		    attribs.UV = vertexUV;
		    gl_Position = model * vec4(position, 1.0f);
		}
	).";

const string ImpostorCreator::fragmentShader =
R".(
		#version 430
		uniform layout(binding = 0) sampler2D diffTex;
		in vs_output_interface {
		    vec2 UV;
		}attribs;
		out vec4 fragColor;
		void main()
		{
			vec4 texel = texture(diffTex, attribs.UV);
			if (texel.a < 0.5) {
				discard;
			}
			fragColor = texel;
		}
	).";

ImpostorCreator::ImpostorCreator(shared_ptr<Context> gl) 
	: VT(new ImpostorCreatorVT())
	, gl(gl)
{
	shared_ptr<Shader> vs = make_shared<Shader>(GL_VERTEX_SHADER, vertexShader);
	shared_ptr<Shader> fs = make_shared<Shader>(GL_FRAGMENT_SHADER, fragmentShader);
	
	shaderProgram = std::make_shared<Program>(vs, fs);
	VT->program = shaderProgram;
	VT->gl = gl;
}

shared_ptr<Impostor> ImpostorCreator::createImpostor(shared_ptr<Program> impostorProgram, shared_ptr<GLModel> glmodel, vec2 textureResolution, int numberOfRows, const int COORD_TYPE)
{
	VT->setModel(glmodel);
	VT->processModel();

	shared_ptr<Texture> texture = setUpTexture(textureResolution, numberOfRows);
	
	// setup render buffer
	std::shared_ptr<Renderbuffer> renderBuffer(std::make_shared<Renderbuffer>(GL_DEPTH_COMPONENT, textureResolution.x, textureResolution.y, 0));
	
	// setup frame buffer
	std::shared_ptr<Framebuffer> frameBuffer(std::make_shared<Framebuffer>(false));
	frameBuffer->bind(GL_FRAMEBUFFER);
	frameBuffer->attachRenderbuffer(GL_DEPTH_ATTACHMENT, renderBuffer);
	frameBuffer->drawBuffer(GL_COLOR_ATTACHMENT0);
	
	int layer = 0;
	float verticalRot = 90;

	for (int x = 0; x < numberOfRows; x++) {

		float horizontalRot = 0;

		for (int y = 0; y < numberOfRows; y++) {

			frameBuffer->attachTexture(GL_COLOR_ATTACHMENT0, texture, 0, layer);

			if (gl->glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
			{
				std::cout << "Failed to load buffers!" << std::endl;
			}

			setOpenGLState(textureResolution);

			glm::mat4 model = scale(glm::mat4(1.0f), glm::vec3((1 / glmodel->maxSize) * 2, (1 / glmodel->maxSize) * 2, (1 / glmodel->maxSize) * 2));

			model = rotate(model, radians(verticalRot), vec3(-1.0f, 0.0f, 0.0f));
			model = rotate(model, radians(horizontalRot), vec3(0.0f, -1.0f, 0.0f));

			if (COORD_TYPE == CYLINDRICAL) {
				model = glm::translate(model, glm::vec3(0, -(glmodel->maxSize / 2), 0));
			}

			VT->program->setMatrix4fv("model", value_ptr(model));

			VT->draw();
			
			horizontalRot += 360 / numberOfRows;
			layer++;
		}

		verticalRot -= 180 / (numberOfRows - 1);
	}

	frameBuffer->unbind(GL_FRAMEBUFFER);

	gl->glGenerateTextureMipmap(texture->getId());

	return make_shared<Impostor>(GPUBillboard(impostorProgram), gl, glmodel, texture, numberOfRows);
}

std::shared_ptr<ge::gl::Texture> ImpostorCreator::setUpTexture(glm::vec2 textureResolution, int numberOfRows)
{
	// setup texture
	std::shared_ptr<ge::gl::Texture> tex(std::make_shared<ge::gl::Texture>(GL_TEXTURE_2D_ARRAY, GL_RGBA8, 0, textureResolution.x, textureResolution.y, numberOfRows * numberOfRows));
	gl->glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	gl->glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	gl->glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	gl->glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	return tex;
}

void ImpostorCreator::setOpenGLState(vec2 textureResolution)
{
	gl->glViewport(0, 0, textureResolution.x, textureResolution.y);
	gl->glClearColor(0.0, 0.0, 0.0, 0.0);
	gl->glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}


/// getters & setters
std::shared_ptr<ge::gl::Program> ImpostorCreator::getShaderProgram() { return this->shaderProgram; }
void ImpostorCreator::setShaderProgram(std::shared_ptr<ge::gl::Program> shaderProgram) { this->shaderProgram = shaderProgram; }

std::shared_ptr<ge::gl::Context> ImpostorCreator::getContext() { return this->gl; }
void ImpostorCreator::setContext(std::shared_ptr<ge::gl::Context> gl) { this->gl = gl; }

