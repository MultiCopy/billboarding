in layout(location = 0) vec3 position;
in layout(location = 1) vec2 vertexUV;

out vs_output_interface
{
	vec2 UV;
}attribs;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main()
{
	attribs.UV = vertexUV;
	gl_Position = projection * computeBillboardPosition(position, model, view);
}