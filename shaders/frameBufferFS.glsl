#version 430 core

uniform layout(binding = 0) sampler2D diffTex;

in vs_output_interface {
    vec2 UV;
}attribs;

layout(location = 0) out vec4 color;


void main()
{
	vec4 texel = texture(diffTex, attribs.UV);
	
	if (texel.a < 0.5) {
		discard;
	}
	
	color = texel;
}