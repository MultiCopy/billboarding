#version 430

uniform layout(binding = 0) sampler2D diffTex;

in vs_output_interface {
    vec2 UV;
}attribs;

out vec4 fragColor;

void main()
{
	vec4 texel = texture(diffTex, attribs.UV);
	
	if (texel.a < 0.5) {
		discard;
	}
	
	fragColor = texel;
}