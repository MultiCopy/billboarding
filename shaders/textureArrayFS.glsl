#version 430 core

uniform layout(binding = 0) sampler2DArray textureArray;

in vs_output_interface
{
	vec2 UV;
}attribs;

in float layer;

out vec4 fragColor;

void main()
{
	vec4 texel = texture(textureArray, vec3(attribs.UV, layer));
	
	if (texel.a < 0.5) {
		discard;
	}

	fragColor = texel;
}