#pragma once

#include <memory>

#include <QWidget>
#include <QSlider>
#include <QLabel>
#include <QPushButton>
#include <QDialog>
#include <QSurfaceFormat>

#include <geGL/Program.h>

namespace ge
{
	namespace gl
	{
		class Context;
		class VertexArray;
		class Texture;
	}

	namespace util
	{
		class OrbitCamera;
		class PerspectiveCamera;
	}

	namespace ad
	{
		class GPUBillboard;
	}
}

namespace app 
{
	namespace gui
	{
		class ImpostorDialog;
	}

	namespace mw
	{
		class OpenGLWindow;

		class GLWidget : public QWidget
		{
			public:
				explicit GLWidget(app::mw::OpenGLWindow* glWindow, QWidget *parent = 0);
				~GLWidget();

			private slots:
				void popupDialog();
				void generateImpostor();

			private:
				QPushButton * impBtn;
				gui::ImpostorDialog* impDialog;
		};
	}
}
