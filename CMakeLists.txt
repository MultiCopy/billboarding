cmake_minimum_required(VERSION 3.7)
project(BillboardApp)

set(APP_NAME "BillboardApp")

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_AUTOMOC ON)

SET_PROPERTY(GLOBAL PROPERTY USE_FOLDERS ON)

find_package(GPUEngine COMPONENTS REQUIRED geUtil)
find_package(GPUEngine COMPONENTS REQUIRED geGL)
find_package(GPUEngine COMPONENTS REQUIRED geSG)
find_package(Qt5 COMPONENTS REQUIRED Gui)
find_package(Qt5 COMPONENTS REQUIRED Widgets)
find_package(BillboardingLibrary COMPONENTS REQUIRED Billboarding)

find_package(AssimpModelLoader HINTS ${GPUEngine_SOURCE_DIR}/geAd/AssimpModelLoader/cmake)
find_package(QtImageLoader QUIET HINTS ${GPUEngine_SOURCE_DIR}/geAd/QtImageLoader/cmake)

set(QTGESG_SOURCES
	QTgeSG/SimpleVT.cpp QTgeSG/SimpleVT.h
	QTgeSG/VisualizationTechnique.h
)

set(GLSG_SOURCES
	glsg/EnumToGL.cpp glsg/EnumToGL.h
	glsg/GLSceneProcessor.cpp glsg/GLSceneProcessor.h
	glsg/GLModel.h
	glsg/GLAttribute.h
)

set(MODULE_IMPOSTOR_SOURCES
	impostor/Impostor.cpp impostor/Impostor.h
	impostor/ImpostorCreator.cpp impostor/ImpostorCreator.h
	impostor/ImpostorCreatorVT.cpp impostor/ImpostorCreatorVT.h
)

set(GUI_SOURCES
	gui/ImpostorDialog.cpp gui/ImpostorDialog.h
)

set(sources
	main.cpp
	RenderManager.cpp RenderManager.h
	GLWidget.cpp GLWidget.h
	OpenGLWindow.cpp OpenGLWindow.h
)

source_group(glsg FILES ${GLSG_SOURCES})
source_group(QTgeSG FILES ${QTGESG_SOURCES})
source_group(impostor FILES ${MODULE_IMPOSTOR_SOURCES})
source_group(gui FILES ${GUI_SOURCES})

set(DEFAULT_RESOERCES_PATH "${CMAKE_CURRENT_LIST_DIR}/resources")
set(${APP_NAME}_RESOURCES "${DEFAULT_RESOERCES_PATH}" CACHE PATH "Relative or absolute path to Application resources.")

find_file(billboardVertexShader billboardVertexShader.glsl
	HINTS ${CMAKE_CURRENT_LIST_DIR}/shaders
)

find_file(impostorVertexShader impostorVertexShader.glsl
	HINTS ${CMAKE_CURRENT_LIST_DIR}/shaders
)

find_file(vertexShader vertexShader.glsl
	HINTS ${CMAKE_CURRENT_LIST_DIR}/shaders
)

find_file(textureArrayFS textureArrayFS.glsl
	HINTS ${CMAKE_CURRENT_LIST_DIR}/shaders
)

find_file(frameBufferFS frameBufferFS.glsl
	HINTS ${CMAKE_CURRENT_LIST_DIR}/shaders
)

find_file(fragmentShader fragmentShader.glsl
	HINTS ${CMAKE_CURRENT_LIST_DIR}/shaders
)

add_executable(${APP_NAME} ${sources} ${GLSG_SOURCES} ${QTGESG_SOURCES} ${MODULE_IMPOSTOR_SOURCES} ${GUI_SOURCES})
target_link_libraries(${APP_NAME} Qt5::Gui Qt5::Widgets geGL geUtil geSG Billboarding QtImageLoader AssimpModelLoader assimp::assimp)
set_target_properties(${APP_NAME} PROPERTIES COMPILE_DEFINITIONS "APP_RESOURCES=\"${${APP_NAME}_RESOURCES}\"")
target_include_directories(${APP_NAME} PUBLIC ${CMAKE_CURRENT_LIST_DIR})
target_compile_definitions(
	${APP_NAME} PUBLIC
		"VERTEX_SHADER=\"${vertexShader}\""
		"BILLBOARD_VERTEX_SHADER=\"${billboardVertexShader}\""
		"IMPOSTOR_VERTEX_SHADER=\"${impostorVertexShader}\"" 
		"TEXTURE_ARRAY_FS=\"${textureArrayFS}\""
		"FRAME_BUFFER_FS=\"${frameBufferFS}\""
		"FRAGMENT_SHADER=\"${fragmentShader}\""
)
set_property(TARGET ${APP_NAME} PROPERTY FOLDER "BillboardApp")

# setting up the MSVC helper var
get_target_property(Qt5dllPath Qt5::Gui IMPORTED_LOCATION_RELEASE)
get_filename_component(Qt5dllDir ${Qt5dllPath} DIRECTORY)
get_target_property(assdllPath assimp::assimp IMPORTED_LOCATION_RELEASE)
get_filename_component(assdllDir ${assdllPath} DIRECTORY)
get_target_property(GPUEdllPath geGL IMPORTED_LOCATION_RELEASE)
set_target_properties(${APP_NAME} PROPERTIES VS_GLOBAL_DllPath "${OUTPUT_BINDIR};${assdllDir}")