//
// Created by forry on 24.11.1517.
//

#include <OpenGLWindow.h>

#include <QtGui/QOpenGLContext>
#include <QTimer>
#include <QImage>
#include <QString>
#include <QtGui>

#include <iostream>
#include <ctime>
#include <cstdlib>
#include <random>

#include <geGL/geGL.h>
#include <geGL/Texture.h>
#include <geUtil/Text.h>
#include <geSG/Scene.h>

#include <AssimpModelLoader.h>
#include <QtImageLoader.h>

#include <glsg/EnumToGL.h>
#include <glsg/GLModel.h>
#include <glsg/GLSceneProcessor.h>
#include <QTgeSG/SimpleVT.h>
#include <impostor/ImpostorCreator.h>
#include <impostor/ImpostorCreatorVT.h>

#include <RenderManager.h>

using namespace std;
using namespace glm;
using namespace app::mw;
using namespace app::mi;
using namespace ge::gl;
using namespace ge::util;
using namespace ge::sg;

vector<float> OpenGLWindow::squarePos = {
	-1.0f,  1.0f, 0.0f, 1.0f, 1.0f, // 0
	 1.0f,  1.0f, 0.0f, 0.0f, 1.0f, // 1
	 1.0f, -1.0f, 0.0f, 0.0f, 0.0f, // 2
	-1.0f, -1.0f, 0.0f, 1.0f, 0.0f  // 3
};

vector<int> OpenGLWindow::squareIndices = {
	0, 1, 2,
	2, 3, 0
};

//! [ctor]
OpenGLWindow::OpenGLWindow(QWindow *parent)
	: QWindow(parent)
	, initialized(false)
	, context(nullptr)
	, billboard(nullptr)
	, texture(nullptr)
	, updateScene(false)
	, VT(new fsg::SimpleVT())
	, impVT(new ImpostorCreatorVT())
{
   setSurfaceType(QWindow::OpenGLSurface); //this needs to be set otherwise makeCurrent and other gl context related functions will fail
   surfaceFormat.setVersion(4, 5);
   surfaceFormat.setProfile(QSurfaceFormat::CoreProfile);

   srand(time(nullptr));

   QTimer *timer = new QTimer(this);
   connect(timer, SIGNAL(timeout()), this, SLOT(renderNow()));
   timer->start(1);
}
//! [ctor]

OpenGLWindow::~OpenGLWindow()
{
}

/**
 * Create OpenGL context with Qt with appropriate surface format.
 * Then initialize geGL and creating geGL context wrapper with OpenGL
 * functions entry points. Also prints out the GL_VERSION string.
 */
void OpenGLWindow::initialize()
{
   if (initialized) return;
   //! [qt_context]
   if (!context)
   {
      context = new QOpenGLContext(this);
      context->setFormat(surfaceFormat);
      bool success = context->create();
      if (!success)
      {
         
      }
   }
   context->makeCurrent(this);
   
   init();
   gl = make_shared<Context>();

   initPrograms();

   VT->gl = gl;
   impVT->gl = gl;
   VT->program = classicProgram;
   impVT->program = classicProgram;

   gl->glEnable(GL_DEPTH_TEST);
   gl->glDisable(GL_CULL_FACE);

   positions = make_shared<Buffer>(squarePos.size() * sizeof(float), squarePos.data()/*, GL_STATIC_DRAW */);
   elementBuffer = make_shared<Buffer>(squareIndices.size() * sizeof(int), squareIndices.data()/*, GL_STATIC_DRAW */);

   loadModels();
   //texture = RenderManager::loadTexture(QString (APP_RESOURCES"/arrow2.png"));
   
   orbitCamera = OrbitCamera();
   orbitCamera.setDistance(200.0f);
   perspectiveCamera = PerspectiveCamera((float) width() / (float) height());
   
   VAO = make_shared<VertexArray>();

   VAO->bind();
   VAO->addElementBuffer(elementBuffer);
   VAO->addAttrib(positions, 0, 3, GL_FLOAT, 5 * sizeof(float), 0);
   VAO->addAttrib(positions, 1, 2, GL_FLOAT, 5 * sizeof(float), 3 * sizeof(float));
   VAO->unbind();

   initialized = true;
}

//! [render]

void OpenGLWindow::render()
{
	RenderManager::setUpGLState(gl, vec2(width(), height()));
	
	switch (RenderManager::state) {
		case RenderManager::State::default:
			{
				mat4 model3D = scale(mat4(1.0f), vec3((1 / glmodel->maxSize) * 2, (1 / glmodel->maxSize) * 2, (1 / glmodel->maxSize) * 2));
				model3D = rotate(model3D, radians(0.0f), vec3(-1.0f, 0.0f, 0.0f));
				model3D = translate(model3D, vec3(0, -(glmodel->maxSize / 2), 0));

				impVT->program->setMatrix4fv("model", value_ptr(model3D));
				impVT->draw();
			}
			break;

		case RenderManager::State::impostor:
			{
				if (impostor == nullptr) {
					ImpostorCreator impCreator = ImpostorCreator(gl);
					impostor = impCreator.createImpostor(impostorProgram, glmodel, vec2(512.0f, 512.0f), RenderManager::impostorInfo.numberOfRows, ImpostorCreator::CoordsType::CYLINDRICAL);
				}

				impostor->getShaderProgram()->setMatrix4fv("view", value_ptr(orbitCamera.getView()))
					->setMatrix4fv("projection", value_ptr(perspectiveCamera.getProjection()));
				impostor->getShaderProgram()->use();
				impostor->getShaderProgram()->set1i("numberOfRows", impostor->getNumberOfRows());
				impostor->recomputeImpostor(orbitCamera.getView(), scale(mat4(1.0f), vec3(75.0f, 75.0f, 75.0f)));
				VAO->bind();
				impostor->getTextureArray()->bind(0);
				impostor->gl->glDrawElements(GL_TRIANGLE_FAN, 6, GL_UNSIGNED_INT, nullptr);
				impostor->getTextureArray()->unbind(0);
				VAO->unbind();
			}
			break;
	}

	context->makeCurrent(this);
    context->swapBuffers(this);  
}

//! [render]

void OpenGLWindow::printError() const
{
   auto err = this->gl->glGetError();
   if(err != GL_NO_ERROR)
   {
      cout << err << endl;
   }
}

//! [renderNow]
void OpenGLWindow::renderNow()
{
   if (!isExposed()) return;
   if (!initialized) initialize();

   if (updateScene)
   {
	   glmodel = ge::glsg::GLSceneProcessor::processScene(scene, gl);
	   VT->setScene(glmodel);
	   VT->processScene();
	   impVT->setModel(glmodel);
	   impVT->processModel();
	   updateScene = false;
   }

   render();
}
//! [renderNow]

//! [eventFilter]
bool OpenGLWindow::event(QEvent *event)
{

   switch (event->type())
   {
	  case QEvent::UpdateRequest:
         renderNow();
         return true;
      default:
         return QWindow::event(event);
   }
}
//! [eventFilter]

//! [expose]
void OpenGLWindow::exposeEvent(QExposeEvent *event)
{
   if (isExposed())
   {
      renderNow();
   }
}
//! [expose]

void OpenGLWindow::mouseMoveEvent(QMouseEvent *event) {

	QPoint point = mapFromGlobal(QPoint(event->x(), event->y()));
	
	if (mouseBefore.length() != 0) {
		vec2 mouse = vec2(point.y(), point.x()) - mouseBefore;

		orbitCamera.addAngles(mouse * 0.02f);

		renderNow();
	}
	
	mouseBefore = vec2(point.y(), point.x());
}

void OpenGLWindow::keyPressEvent(QKeyEvent *event) {

	if (event->key() == Qt::Key_W) {
		orbitCamera.setDistance(orbitCamera.getDistance() - 10);
	}
	if (event->key() == Qt::Key_S) {
		orbitCamera.setDistance(orbitCamera.getDistance() + 10);
	}

	renderNow();
}

void OpenGLWindow::setScene(shared_ptr<Scene> scene) {

	this->scene = scene;
	updateScene = true;
}

void OpenGLWindow::initPrograms()
{
	// load classic shader program
	shared_ptr<Shader> vertexShader = make_shared<Shader>(GL_VERTEX_SHADER, loadTextFile(VERTEX_SHADER));
	shared_ptr<Shader> fragmentShader = make_shared<Shader>(GL_FRAGMENT_SHADER, loadTextFile(FRAGMENT_SHADER));
	
	shared_ptr<Shader> textureArrayFS = make_shared<Shader>(GL_FRAGMENT_SHADER, loadTextFile(TEXTURE_ARRAY_FS));
	textureArrayProgram = make_shared<Program>(vertexShader, textureArrayFS);

	shared_ptr<Shader> vs = make_shared<Shader>(GL_VERTEX_SHADER, ImpostorCreator::vertexShader);
	shared_ptr<Shader> fs = make_shared<Shader>(GL_FRAGMENT_SHADER, loadTextFile(FRAGMENT_SHADER));
	classicProgram = make_shared<Program>(vs, fs);
	
	//load billboard shader program
	string billboardShader = "#version 430\n" + ge::ad::GPUBillboard::computeBillboardGLPosition + loadTextFile(BILLBOARD_VERTEX_SHADER);
	shared_ptr<Shader> billboardVertexShader = make_shared<Shader>(GL_VERTEX_SHADER, billboardShader);
	billboardProgram = make_shared<Program>(billboardVertexShader, fragmentShader);

	// load impostor shader program
	string impostorShader = "#version 430\n" + ge::ad::GPUBillboard::computeBillboardGLPosition + Impostor::computeTextureIndex + loadTextFile(IMPOSTOR_VERTEX_SHADER);
	shared_ptr<Shader> impostorVertexShader = make_shared<Shader>(GL_VERTEX_SHADER, impostorShader);
	impostorProgram = make_shared<Program>(impostorVertexShader, textureArrayFS);

	// load frame buffer program
	shared_ptr<Shader> frameBufferFS = make_shared<Shader>(GL_FRAGMENT_SHADER, loadTextFile(FRAME_BUFFER_FS));
	frameBufferProgram = make_shared<Program>(billboardVertexShader, frameBufferFS);
}


void OpenGLWindow::loadModels() {
	
	models.emplace_back(translate(mat4(1.0f), vec3(100.0f, 0.0f, 0.0f)));
	models.emplace_back(translate(mat4(1.0f), vec3(-100.0f, 0.0f, 0.0f)));

	/*for (int i = 0; i < 250; i++)
	{
		models.emplace_back(translate(mat4(1.0f), vec3(rand() % 400 - 200, 0, rand() % 400 - 200)));
	}*//*
	models.emplace_back(translate(mat4(1.0f), vec3(200, 0, 0)));

	models.emplace_back(translate(mat4(1.0f), vec3(-50, 50, -150)));
	models.emplace_back(translate(mat4(1.0f), vec3(50, 50, -150)));
	models.emplace_back(translate(mat4(1.0f), vec3(-50, -50, -150)));
	models.emplace_back(translate(mat4(1.0f), vec3(50, -50, -150)));

	models.emplace_back(translate(mat4(1.0f), vec3(-50, 50, -50)));
	models.emplace_back(translate(mat4(1.0f), vec3(50, 50, -50)));
	models.emplace_back(translate(mat4(1.0f), vec3(-50, -50, -50)));
	models.emplace_back(translate(mat4(1.0f), vec3(50, -50, -50)));

	models.emplace_back(translate(mat4(1.0f), vec3(-50, 50, 50)));
	models.emplace_back(translate(mat4(1.0f), vec3(50, 50, 50)));
	models.emplace_back(translate(mat4(1.0f), vec3(-50, -50, 50)));
	models.emplace_back(translate(mat4(1.0f), vec3(50, -50, 50)));

	models.emplace_back(translate(mat4(1.0f), vec3(-50, 50, 150)));
	models.emplace_back(translate(mat4(1.0f), vec3(50, 50, 150)));
	models.emplace_back(translate(mat4(1.0f), vec3(-50, -50, 150)));
	models.emplace_back(translate(mat4(1.0f), vec3(50, -50, 150)));

	////////////////////////////////////////////////////////////////////////////////////////

	models.emplace_back(translate(mat4(1.0f), vec3(-150, 50, -150)));
	models.emplace_back(translate(mat4(1.0f), vec3(150, 50, -150)));
	models.emplace_back(translate(mat4(1.0f), vec3(-150, -50, -150)));
	models.emplace_back(translate(mat4(1.0f), vec3(150, -50, -150)));

	models.emplace_back(translate(mat4(1.0f), vec3(-150, 50, -50)));
	models.emplace_back(translate(mat4(1.0f), vec3(150, 50, -50)));
	models.emplace_back(translate(mat4(1.0f), vec3(-150, -50, -50)));
	models.emplace_back(translate(mat4(1.0f), vec3(150, -50, -50)));

	models.emplace_back(translate(mat4(1.0f), vec3(-150, 50, 50)));
	models.emplace_back(translate(mat4(1.0f), vec3(150, 50, 50)));
	models.emplace_back(translate(mat4(1.0f), vec3(-150, -50, 50)));
	models.emplace_back(translate(mat4(1.0f), vec3(150, -50, 50)));

	models.emplace_back(translate(mat4(1.0f), vec3(-150, 50, 150)));
	models.emplace_back(translate(mat4(1.0f), vec3(150, 50, 150)));
	models.emplace_back(translate(mat4(1.0f), vec3(-150, -50, 150)));
	models.emplace_back(translate(mat4(1.0f), vec3(150, -50, 150)));

	////////////////////////////////////////////////////////////////////////////////////////

	models.emplace_back(translate(mat4(1.0f), vec3(-50, 150, -150)));
	models.emplace_back(translate(mat4(1.0f), vec3(50, 150, -150)));
	models.emplace_back(translate(mat4(1.0f), vec3(-50, -150, -150)));
	models.emplace_back(translate(mat4(1.0f), vec3(50, -150, -150)));

	models.emplace_back(translate(mat4(1.0f), vec3(-50, 150, -50)));
	models.emplace_back(translate(mat4(1.0f), vec3(50, 150, -50)));
	models.emplace_back(translate(mat4(1.0f), vec3(-50, -150, -50)));
	models.emplace_back(translate(mat4(1.0f), vec3(50, -150, -50)));

	models.emplace_back(translate(mat4(1.0f), vec3(-50, 150, 50)));
	models.emplace_back(translate(mat4(1.0f), vec3(50, 150, 50)));
	models.emplace_back(translate(mat4(1.0f), vec3(-50, -150, 50)));
	models.emplace_back(translate(mat4(1.0f), vec3(50, -150, 50)));

	models.emplace_back(translate(mat4(1.0f), vec3(-50, 150, 150)));
	models.emplace_back(translate(mat4(1.0f), vec3(50, 150, 150)));
	models.emplace_back(translate(mat4(1.0f), vec3(-50, -150, 150)));
	models.emplace_back(translate(mat4(1.0f), vec3(50, -150, 150)));

	////////////////////////////////////////////////////////////////////////////////////////

	models.emplace_back(translate(mat4(1.0f), vec3(-150, 150, -150)));
	models.emplace_back(translate(mat4(1.0f), vec3(150, 150, -150)));
	models.emplace_back(translate(mat4(1.0f), vec3(-150, -150, -150)));
	models.emplace_back(translate(mat4(1.0f), vec3(150, -150, -150)));

	models.emplace_back(translate(mat4(1.0f), vec3(-150, 150, -50)));
	models.emplace_back(translate(mat4(1.0f), vec3(150, 150, -50)));
	models.emplace_back(translate(mat4(1.0f), vec3(-150, -150, -50)));
	models.emplace_back(translate(mat4(1.0f), vec3(150, -150, -50)));

	models.emplace_back(translate(mat4(1.0f), vec3(-150, 150, 50)));
	models.emplace_back(translate(mat4(1.0f), vec3(150, 150, 50)));
	models.emplace_back(translate(mat4(1.0f), vec3(-150, -150, 50)));
	models.emplace_back(translate(mat4(1.0f), vec3(150, -150, 50)));

	models.emplace_back(translate(mat4(1.0f), vec3(-150, 150, 150)));
	models.emplace_back(translate(mat4(1.0f), vec3(150, 150, 150)));
	models.emplace_back(translate(mat4(1.0f), vec3(-150, -150, 150)));
	models.emplace_back(translate(mat4(1.0f), vec3(150, -150, 150)));*/
}