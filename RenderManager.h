#pragma once

#include <memory>

#include <QString>

#include <glm/glm.hpp>

namespace ge
{
	namespace gl
	{
		class Context;
		class Texture;
	}
}

namespace app
{
	namespace mw
	{
		class RenderManager
		{
			public:
				enum State { default, impostor };
				static int state;
				
				static std::shared_ptr<ge::gl::Texture> loadTexture(QString texPath, std::shared_ptr<ge::gl::Context> gl);
				static void setRenderingState(int state);
				static void setImpostorInfo(int numberOfRows);

				static void setUpGLState(std::shared_ptr<ge::gl::Context> gl, glm::vec2 viewPortRes);
				static void render();

				static struct ImpostorInfo
				{
					int numberOfRows;
				} impostorInfo;
		};
	}
}

