//
// Created by forry on 24.11.2017.
//

#pragma once

#include <QtGui/QWindow>
#include <memory>
#include <QKeyEvent>

#include <glm/gtc/type_ptr.hpp>

#include <geUtil/OrbitCamera.h>
#include <geUtil/PerspectiveCamera.h>


#include <Billboarding/GPUBillboard.h>
#include <impostor/Impostor.h>
#include <geGL/ProgramPipeline.h>
#include "impostor/ImpostorCreatorVT.h"


class QOpenGLPaintDevice;

namespace fsg {
	class SimpleVT;
}

namespace ge
{
	namespace sg {
		class Scene;
	}

	namespace glsg {
		class GLModel;
	}

	namespace gl {
		class Context;
		class Program;
		class VertexArray;
		class Buffer;
		class Texture;
	}
}

namespace app 
{
   namespace mw
   {

      /**
       * Basic QWindow subclass for simple OpenGL rendering.
       */
      class OpenGLWindow : public QWindow
      {
      Q_OBJECT
      public:
         explicit OpenGLWindow(QWindow *parent = 0);

         ~OpenGLWindow();

         virtual void render();
         virtual void initialize();

         static const std::string fragmentShaderSrc;
		 void setScene(std::shared_ptr<ge::sg::Scene> scene);

      public slots:

         void renderNow();

      protected:
         bool event(QEvent *event) override;
         void exposeEvent(QExposeEvent *event) override;
		 void mouseMoveEvent(QMouseEvent *event) override;
		 void keyPressEvent(QKeyEvent *event) override;
         void printError() const;

      private:
		 void loadModels();
		 void initPrograms();

		 std::shared_ptr<ge::sg::Scene> scene;
		 bool updateScene;

		 //std::shared_ptr<fsg::SimpleVT> VT;
		 std::shared_ptr<fsg::SimpleVT> VT;
		 std::shared_ptr<app::mi::ImpostorCreatorVT> impVT;
		 std::shared_ptr<ge::glsg::GLModel> glmodel;

		 std::shared_ptr<ge::gl::Context> gl;
		 std::shared_ptr<ge::gl::Program> classicProgram;
		 std::shared_ptr<ge::gl::Program> billboardProgram;
		 std::shared_ptr<ge::gl::Program> impostorProgram;
		 std::shared_ptr<ge::gl::Program> frameBufferProgram;
		 std::shared_ptr<ge::gl::Program> textureArrayProgram;
		 std::shared_ptr<ge::gl::VertexArray> VAO;
		 QOpenGLContext *context;
		 QSurfaceFormat surfaceFormat;

         bool initialized;

		 std::shared_ptr<ge::gl::Texture> texture;

		 std::vector<glm::mat4> models;
		 ge::ad::GPUBillboard billboard;
		 std::shared_ptr<app::mi::Impostor> impostor;

		 glm::vec2 mouseBefore;
		 ge::util::OrbitCamera orbitCamera;
		 ge::util::PerspectiveCamera perspectiveCamera;

		 static std::vector<float> squarePos;
		 static std::vector<int> squareIndices;
		 
         std::shared_ptr<ge::gl::Buffer> positions;
         std::shared_ptr<ge::gl::Buffer> elementBuffer;

		 GLuint* data;
      };
   }
}