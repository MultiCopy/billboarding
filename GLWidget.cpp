﻿#include <GLWidget.h>

#include <RenderManager.h>

#include <qsurface.h>
#include <OpenGLWindow.h>

#include <gui/ImpostorDialog.h>


using namespace std;
using namespace glm;
using namespace app::mw;
using namespace app::gui;
using namespace ge::gl;
using namespace ge::util;
using namespace ge::ad;

GLWidget::GLWidget(OpenGLWindow* glWindow, QWidget* parent) 
	: QWidget(parent)
	, impDialog(new ImpostorDialog())
{
	QWidget *glWidget = createWindowContainer(glWindow);
	glWidget->setFocusPolicy(Qt::TabFocus);

	auto mainLayout = new QHBoxLayout;
	mainLayout->addWidget(glWidget);

	impBtn = new QPushButton(tr("Vygenerovať Impostor"), this);
	impBtn->setFixedWidth(150);
	connect(impBtn, &QPushButton::clicked, this, &GLWidget::popupDialog);
	mainLayout->addWidget(impBtn);

	this->setLayout(mainLayout);

	connect(impDialog->ok, &QPushButton::clicked, this, &GLWidget::generateImpostor);

	setWindowTitle(tr("Demonštračná aplikácia"));
}

void GLWidget::popupDialog()
{
	impDialog->show();
}


void GLWidget::generateImpostor()
{
	RenderManager::setRenderingState(RenderManager::State::impostor);
	RenderManager::setImpostorInfo(impDialog->getSliderValue());
	impDialog->close();
}

GLWidget::~GLWidget()
{
	
}







