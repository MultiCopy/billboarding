﻿#include <gui/ImpostorDialog.h>

using namespace std;
using namespace app::gui;

ImpostorDialog::ImpostorDialog(QWidget* parent) : QDialog(parent)
{
	initDialogComponents();

	auto layout = setupLayout();

	this->setFixedWidth(350);
	this->setFixedHeight(150);
	this->setLayout(layout);

	this->setWindowTitle("Nastavenia Impostor");
}

QVBoxLayout* ImpostorDialog::setupLayout()
{
	auto labeledSlider = new QHBoxLayout;
	labeledSlider->addWidget(slider);
	labeledSlider->addWidget(label);

	auto buttons = new QHBoxLayout;
	buttons->addWidget(ok);
	buttons->addWidget(cancel);

	QWidget* lsLayout = new QWidget;
	lsLayout->setLayout(labeledSlider);
	QWidget* bLayout = new QWidget;
	bLayout->setLayout(buttons);

	auto mainLayout = new QVBoxLayout;
	mainLayout->addWidget(lsLayout);
	mainLayout->addWidget(bLayout);

	return mainLayout;
}


void ImpostorDialog::initDialogComponents()
{
	slider = createSlider();
	slider->setValue(10);

	label = createLabel();
	label->setText(QString::number(10));

	connect(slider, &QSlider::valueChanged, this, &ImpostorDialog::sliderValue);

	ok = new QPushButton(tr("OK"), this);
	cancel = new QPushButton(tr("Zrušiť"), this);
	connect(cancel, &QPushButton::clicked, this, &ImpostorDialog::closeDialog);
}


QSlider* ImpostorDialog::createSlider()
{
	auto slider = new QSlider(Qt::Horizontal);
	slider->setRange(5, 20);
	slider->setSingleStep(1);
	slider->setPageStep(5);
	slider->setTickInterval(1);
	slider->setFixedWidth(250);
	slider->setTickPosition(QSlider::TicksRight);

	return slider;
}

QLabel* ImpostorDialog::createLabel()
{
	auto label = new QLabel();
	label->setFrameStyle(QFrame::StyledPanel | QFrame::Sunken);
	label->setFixedWidth(30);
	label->setFixedHeight(30);
	label->setAlignment(Qt::AlignCenter);
	label->setTextFormat(Qt::TextFormat::PlainText);
	return label;
}

void ImpostorDialog::sliderValue(int value)
{
	label->setText(QString::number(value));
}

void ImpostorDialog::closeDialog()
{
	this->close();
}

int ImpostorDialog::getSliderValue()
{
	return slider->value();
}


/*
QDialog* GLWidget::createDialog()
{
	auto mainLayout = new QVBoxLayout;
	auto container = new QHBoxLayout;

	container->addWidget(impostorSlider);
	container->addWidget(impostorLabel);

	QWidget* w = new QWidget;
	w->setLayout(container);
	mainLayout->addWidget(w);

	auto dialog = new QDialog();
	dialog->setFixedWidth(500);
	dialog->setFixedHeight(300);
	dialog->setLayout(mainLayout);

	return dialog;
}
*/