#pragma once

#include <QDialog>
#include <QSlider>
#include <QLabel>
#include <QPushBUtton>
#include <QLayout>

namespace app
{
	namespace gui
	{
		class ImpostorDialog : public QDialog
		{
			Q_OBJECT

			public:
				QPushButton* ok;
				QPushButton* cancel;

				ImpostorDialog(QWidget* parent = nullptr);

				int getSliderValue();

			public slots:
				void sliderValue(int value);
				void closeDialog();

			private:
				QSlider* slider;
				QLabel* label;

				void initDialogComponents();
				QSlider* createSlider();
				QLabel* createLabel();
				QVBoxLayout* setupLayout();
			
		};
	}
}
